---
layout: markdown_page
title: "Sourcing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Purpose of Sourcing

The speed with which we can grow our team is dependent on the rate at which we
get qualified applicants. If we have capacity to interview more applicants than
we are receiving organically, we may resort to active sourcing in an attempt to
close the gap.

The purpose of sourcing is to generate a large list of qualified potential
candidates. From this list, Recruiting will solicit interest, and we hope to
increase the number of qualified candidates in our pipeline.

## Sourcing vs. Referrals

Sourcing and [making
referrals](/handbook/hiring/lever/#how-to-make-a-referral)
are similar activities, because they both involve recommending people to join
the company. They differ in familiarity and confidence, however:

- If you make a **referral**, you are claiming a personal relationship with the
  candidate that enables you to make a very confident claim about their cultural
  fit with GitLab and their ability to excel at the role in question.
- If you **source** a candidate, you do not have to know them or have any
  confidence in their ability to excel at the role beyond "they seem qualified."

Because of this difference, sourcing is usually not a very detailed activity,
and it is common for a person to source dozens of candidates in a brief session
where they may rarely if ever make a referral. This also means that [referral
bonuses](/handbook/incentives/#referral-bonuses) and
other incentive programs for referrals do not apply to sourced candidates.

## How to Source Candidates

The most efficient way to source candidates for any position is to search
through a professional network, such as LinkedIn, AngelList, etc. for people who
match the skillset and job history that you are looking for. For some positions,
other networks may prove useful as well - for example, someone sourcing for a
developer role could search GitLab profiles to identify promising candidates.
Professional networks however make it easy to scan a person's employment history
quickly and efficiently and are designed to present their best impression to
potential employers. Because of this, they are a very efficient tool for finding
potential candidates.

When you have identified someone as a good potential candidate, send their
profile along with any requested information to your sourcing partner in
recruiting so they can reach out to the candidate.

## Example: Sourcing Developers

Here's an example workflow for sourcing developer candidates:

1. In LinkedIn, search for `"ruby on rails" developer`. LinkedIn presents you
   with a list of matching profiles.
1. Based on their current company/job title, open any profiles that look
   interesting in a new tab.
1. Review the candidate's job history and skillset for positive indicators,
   like:
     * Multiple endorsements for Ruby
     * Tenure at established/strong Rails companies
     * Startup/product experience
     * ...or anything else that makes them seem likely to be a fit for our team
1. If they seem like a good fit, pass their information along to Recruiting.

Following this method - and with practice scanning profiles - it's possible to
find a few dozen potential good candidates in 30 minutes or less.

## Source-A-Thons

Because of the high impact that sourcing activities can have on the team's
ability to scale, teams that are growing rapidly may set aggressive sourcing
goals and schedule "Source-A-Thons" to help achieve them quickly. A
Source-A-Thon is a roughly 30-minute meeting that facilitates sourcing in a
group. While sourcing can be performed asynchronously, meetings help by:

* Making it a social activity, which helps alleviate the otherwise tedious
  nature of scanning online resumes.
* Enabling quick sharing of insights, as people may discover productive search
  terms or interesting profiles.
* Facilitating learning about the sourcing process, as people who are relatively
  new to sourcing can learn from more experienced team members.

## What We Do With Potential Candidates

When you source a candidate and provie their information to our recruiting team,
you can expect that they will receive an email asking them if they’re interested
in talking about an opportunity to join our team. You won’t be involved or
mentioned in this process aside from having passed their information along.

We do try to personalize these emails as much as feasibly possible so that they
are not impersonal or "spammy," and we rely on the team sourcing to identify
relevant and qualified candidates to ensure these messages are as meaningful as
possible.
